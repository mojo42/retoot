#!/bin/bash
env_file=$1

# Just a small script to setup & run retoot with several accounts

# Test dependancies
python3 --help &> /dev/null || ( echo "python3 not found" ; exit 1 )
virtualenv --help &> /dev/null || ( echo "python3 not found" ; exit 1 )

# Init or source virtualenv
r=$(cd "$(dirname $0)" && pwd)
if [ -d $r/retoot_env ]; then
    source $r/retoot_env/bin/activate
else
    echo "creating virtual env..."
    virtualenv -p python3 $r/retoot_env
    source $r/retoot_env/bin/activate
    python3 $r/setup.py install
fi

if [ -f "$env_file" ]; then
    set -a
    source $env_file
    set +a
fi

while ((42)); do
    python3 $r/retoot/retoot.py
    echo "Retoot exited with status $?"
    echo "Sleeping few seconds before restart ..."
    sleep 30
done
