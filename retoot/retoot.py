#!/usr/bin/env python3

# Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from this
# software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import getpass
import tweepy
import time
import json
import os
import re
import urllib.request
import xml.sax.saxutils as saxutils
from tweepy import OAuthHandler
from mastodon import Mastodon

class Mirror():
    def __init__(self):
        self.instance = None
        self.id = None
        self.secret = None
        self.token = None
        self.api = None
        self.target = None
        self.t_userid = None

    def ready(self):
        return self.instance and self.id and self.secret and self.token and self.target

    def connect(self):
        url = "https://" + self.instance
        self.api = Mastodon(client_id=self.id,
                            client_secret=self.secret,
                            access_token=self.token,
                            api_base_url=url)
        check = self.api.account_verify_credentials()
        if check.get('error'):
            return False
        else:
            self.username = str(check['username'])
            print('connected on @' + self.username + '@' + self.instance)
            return True
    def __str__(self):
        s = "instance: <" + str(self.instance) + ">\n" + \
            "id: <" + str(self.id) + ">\n" + \
            "secret: <" + str(self.secret) + ">\n" + \
            "token: <" + str(self.token) + ">\n" + \
            "twitter target: <" + str(self.target) + ">"
        return s

def mastodon_get_creds():
    print('Mastodon API credential creation:')
    instance = input("Mastodon instance: ")
    email = input("Email used to login: ")
    password = getpass.getpass()
    base_url = "https://" + instance
    id, secret = Mastodon.create_app('retoot', api_base_url=base_url)
    api = Mastodon(client_id=id, client_secret=secret, api_base_url=base_url)
    token = api.log_in(email, password)
    r = api.account_verify_credentials()
    if not r.get('error'):
        print('Your Mastodon API credentials:')
        print('id: ' + id)
        print('secret: ' + secret)
        print('token: ' + token)
        print('re-run script with env.list loaded')
        return

class Twitter():
    def __init__(self):
        self.cons_key = os.environ.get('RT_T_CONS_KEY')
        self.cons_secret = os.environ.get('RT_T_CONS_SECRET')
        self.acc_token = os.environ.get('RT_T_ACC_TOKEN')
        self.acc_secret = os.environ.get('RT_T_ACC_SECRET')

    def ready(self):
        return self.cons_key and self.cons_secret and self.acc_token and self.acc_secret

    def connect(self):
        self.auth = OAuthHandler(self.cons_key, self.cons_secret)
        self.auth.set_access_token(self.acc_token, self.acc_secret)
        self.api = tweepy.API(self.auth)

    def get_userid(self, target):
        return self.api.get_user(screen_name=target).id

    def stream(self, mirror_array):
        self.mirrors = mirror_array
        listener = TweetListener(self.mirrors)
        stream = tweepy.Stream(self.auth, listener)
        userid_array = []
        print("now listening to new tweets from:")
        for m in self.mirrors:
            print("@" + m.target + " => @" + m.username + "@" + m.instance)
            m.t_userid = self.get_userid(m.target)
            userid_array.append(str(m.t_userid))
        stream.filter(follow=userid_array)

class TweetListener(tweepy.StreamListener):
    def __init__(self, mirrors, api=None):
        self._mirrors = mirrors
        self.api = None
        if os.environ.get('RT_SHOW_SOURCE'):
            self._source = True
        else:
            self._source = False
        if os.environ.get('RT_NO_URL_EXPANSION'):
            self._expand_urls = False
        else:
            self._expand_urls = True

    @staticmethod
    def expand_urls(status_text):
        urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', status_text)
        for url in urls:
            try:
                res = urllib.request.urlopen(url)
                actual_url = res.geturl()
                status_text = status_text.replace(url, actual_url)
            except:
                None
        return status_text

    def on_status(self, status):
        if status.in_reply_to_user_id:
            return True
        userid = status.user.id
        for m in self._mirrors:
            if userid != m.t_userid:
                continue
            # full_text key will only be set for long (>140 characters) tweets
            try:
                text = status.extended_tweet['full_text']
            except AttributeError:
                text = status.text
            text = saxutils.unescape(text)
            if self._expand_urls:
                text = TweetListener.expand_urls(text)
            if self._source:
                tweet_url = "https://twitter.com/" + m.target + "/status/" + status.id_str
                text += " source: " + tweet_url
            m.api.toot(text)
            print("RETOOT @" + m.target + " => @" + m.username + "@" + m.instance)
            print(text)
            break
        return True

    def on_timeout(self):
        print('Got Timeout')
        return True

    def on_error(self, status):
        print("error status: " + str(status))
        if status == 420:
            print("it seems we are doing too many requests on twitter, sleep few seconds")
            time.sleep(60)
        return True

def get_all_mirrors():
    mirrors = dict()
    for e in os.environ:
        s = re.search(r"^RT__([0-9]+)_(INSTANCE|ID|SECRET|TOKEN|TARGET)$", e)
        if not s:
            continue
        id = s.group(1)
        param = s.group(2)
        value = os.environ[e]
        if not mirrors.get(id):
            mirrors[id] = Mirror()
        if param == 'INSTANCE':
            mirrors[id].instance = value
        elif param == 'ID':
            mirrors[id].id = value
        elif param == 'SECRET':
            mirrors[id].secret = value
        elif param == 'TOKEN':
            mirrors[id].token = value
        elif param == 'TARGET':
            mirrors[id].target = value
    a = []
    for id, m in mirrors.items():
            a.append(m)
    return a

def main():
    mirrors = get_all_mirrors()
    if len(mirrors) == 0:
        mastodon_get_creds()
        return

    for m in mirrors:
        if not m.ready():
            print("Missing parameters:")
            print(m)
            print("----------------------")
            return
        else:
            m.connect()

    t = Twitter()
    if not t.ready():
        print("There are missing twitter dependencies")
        print("check your env configuration or load env.list")
        return
    t.connect()
    t.stream(mirrors)

if __name__ == '__main__':
    main()
